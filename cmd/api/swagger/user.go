package swagger

import (
	"gitlab.com/tylerjkatz/ads-app-demo/internal"

	"gitlab.com/tylerjkatz/ads-app-demo/cmd/api/request"
)

// User update request
// swagger:parameters userUpdate
type swaggUserUpdateReq struct {
	// in:body
	Body request.UpdateUser
}

// User model response
// swagger:response userResp
type swaggUserResponse struct {
	// in:body
	Body struct {
		*model.User
	}
}

// Users model response
// swagger:response userListResp
type swaggUserListResponse struct {
	// in:body
	Body struct {
		Users []model.User `json:"users"`
		Page  int          `json:"page"`
	}
}

// User.Campaigns model response
// swagger:response userCampaignListResp
type swaggUserCampaignListResponse struct {
	// in:body
	Body struct {
		Campaigns []model.Campaign `json:"campaigns"`
		Page      int              `json:"page"`
	}
}
