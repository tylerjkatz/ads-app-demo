package service

import (
	"net/http"
	"time"

	"github.com/labstack/echo"
	"gitlab.com/tylerjkatz/ads-app-demo/cmd/api/request"

	"gitlab.com/tylerjkatz/ads-app-demo/internal/auth"
)

// Auth represents auth http service
type Auth struct {
	svc *auth.Service
}

// NewAuth creates new auth http service
func NewAuth(svc *auth.Service, e *echo.Echo, mw echo.MiddlewareFunc) {
	a := Auth{svc}

	// swagger:route POST /login auth login
	// Logs in user by username and password.
	// responses:
	//  200: loginResp
	//  400: errMsg
	//  401: errMsg
	// 	403: err
	//  404: errMsg
	//  500: err
	e.POST("/login/", a.login)
}

func (a *Auth) login(c echo.Context) error {
	cred, err := request.Login(c)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{
			"message": "invalid login request",
		})
	}

	res, err := a.svc.Authenticate(c, cred.Username, cred.Password)
	if err != nil {
		return err
	}

	//convert auth token expiration to time.Time
	timeExp, _ := time.Parse(time.RFC3339, res.Expires)

	//set jwt cookie needed in header for auth requests
	c.SetCookie(&http.Cookie{
		Name:    "ads_app_demo_auth_token",
		Value:   res.Token,
		Expires: timeExp,
		Path:    "/",
	})
	return c.JSON(http.StatusOK, res)
}
