package service_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/go-pg/pg/orm"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"

	"gitlab.com/tylerjkatz/ads-app-demo/internal/user"

	"gitlab.com/tylerjkatz/ads-app-demo/internal"

	"gitlab.com/tylerjkatz/ads-app-demo/cmd/api/server"
	"gitlab.com/tylerjkatz/ads-app-demo/cmd/api/service"
	"gitlab.com/tylerjkatz/ads-app-demo/internal/auth"
	"gitlab.com/tylerjkatz/ads-app-demo/internal/mock"
	"gitlab.com/tylerjkatz/ads-app-demo/internal/mock/mockdb"
)

func TestListUsers(t *testing.T) {
	type listResponse struct {
		Users []model.User `json:"users"`
		Page  int          `json:"page"`
	}
	cases := []struct {
		name       string
		req        string
		wantStatus int
		wantResp   *listResponse
		udb        *mockdb.User
		rbac       *mock.RBAC
		auth       *mock.Auth
		jwt        *mock.JWT
	}{
		{
			name:       "Invalid request",
			req:        `?limit=2222&page=-1`,
			wantStatus: http.StatusBadRequest,
			jwt: &mock.JWT{
				GenerateTokenFn: func(*model.User) (string, string, error) {
					return "jwttokenstring", mock.TestTime(2018).Format(time.RFC3339), nil
				},
			},
		},
		{
			name: "Fail on query list",
			req:  `?limit=100&page=1`,
			auth: &mock.Auth{
				UserFn: func(c echo.Context) *model.AuthUser {
					return &model.AuthUser{
						ID:   1,
						Role: model.UserRole,
					}
				}},
			wantStatus: http.StatusUnauthorized,
			jwt: &mock.JWT{
				GenerateTokenFn: func(*model.User) (string, string, error) {
					return "jwttokenstring", mock.TestTime(2018).Format(time.RFC3339), nil
				},
			},
		},
		{
			name: "Success",
			req:  `?limit=100&page=1`,
			auth: &mock.Auth{
				UserFn: func(c echo.Context) *model.AuthUser {
					return &model.AuthUser{
						ID:   1,
						Role: model.SuperAdminRole,
					}
				}},
			udb: &mockdb.User{
				ListFn: func(db orm.DB, q *model.ListQuery, p *model.Pagination) ([]model.User, error) {
					if p.Limit == 100 && p.Offset == 100 {
						return []model.User{
							{
								Base: model.Base{
									ID:        10,
									CreatedAt: mock.TestTime(2001),
									UpdatedAt: mock.TestTime(2002),
								},
								Role: &model.Role{
									ID:          1,
									AccessLevel: 1,
									Name:        "SUPER_ADMIN",
								},
							},
							{
								Base: model.Base{
									ID:        11,
									CreatedAt: mock.TestTime(2004),
									UpdatedAt: mock.TestTime(2005),
								},
								Role: &model.Role{
									ID:          2,
									AccessLevel: 2,
									Name:        "ADMIN",
								},
							},
						}, nil
					}
					return nil, model.ErrGeneric
				},
			},
			wantStatus: http.StatusOK,
			wantResp: &listResponse{
				Users: []model.User{
					{
						Base: model.Base{
							ID:        10,
							CreatedAt: mock.TestTime(2001),
							UpdatedAt: mock.TestTime(2002),
						},
						Role: &model.Role{
							ID:          1,
							AccessLevel: 1,
							Name:        "SUPER_ADMIN",
						},
					},
					{
						Base: model.Base{
							ID:        11,
							CreatedAt: mock.TestTime(2004),
							UpdatedAt: mock.TestTime(2005),
						},
						Role: &model.Role{
							ID:          2,
							AccessLevel: 2,
							Name:        "ADMIN",
						},
					},
				},
				Page: 1,
			},
			jwt: &mock.JWT{
				GenerateTokenFn: func(*model.User) (string, string, error) {
					return "jwttokenstring", mock.TestTime(2018).Format(time.RFC3339), nil
				},
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			r := server.New("../../../")
			rg := r.Group("/v1/users")
			service.NewUser(user.New(nil, tt.udb, tt.rbac, tt.auth), auth.New(nil, tt.udb, tt.jwt), rg, nil)
			ts := httptest.NewServer(r)
			defer ts.Close()
			path := ts.URL + "/v1/users" + tt.req
			res, err := http.Get(path)
			if err != nil {
				t.Fatal(err)
			}
			defer res.Body.Close()
			if tt.wantResp != nil {
				response := new(listResponse)
				if err := json.NewDecoder(res.Body).Decode(response); err != nil {
					t.Fatal(err)
				}
				assert.Equal(t, tt.wantResp, response)
			}
			assert.Equal(t, tt.wantStatus, res.StatusCode)
		})
	}
}

func TestViewUser(t *testing.T) {
	cases := []struct {
		name       string
		req        string
		wantStatus int
		wantResp   *model.User
		udb        *mockdb.User
		rbac       *mock.RBAC
		auth       *mock.Auth
		jwt        *mock.JWT
	}{
		{
			name:       "Invalid request",
			req:        `a`,
			wantStatus: http.StatusBadRequest,
			jwt: &mock.JWT{
				GenerateTokenFn: func(*model.User) (string, string, error) {
					return "jwttokenstring", mock.TestTime(2018).Format(time.RFC3339), nil
				},
			},
		},
		{
			name: "Fail on RBAC",
			req:  `1`,
			rbac: &mock.RBAC{
				EnforceUserFn: func(echo.Context, int) error {
					return echo.ErrForbidden
				},
			},
			wantStatus: http.StatusNotFound,
			jwt: &mock.JWT{
				GenerateTokenFn: func(*model.User) (string, string, error) {
					return "jwttokenstring", mock.TestTime(2018).Format(time.RFC3339), nil
				},
			},
		},
		{
			name: "Success",
			req:  `1`,
			rbac: &mock.RBAC{
				EnforceUserFn: func(echo.Context, int) error {
					return nil
				},
			},
			udb: &mockdb.User{
				ViewFn: func(db orm.DB, id int) (*model.User, error) {
					return &model.User{
						Base: model.Base{
							ID:        1,
							CreatedAt: mock.TestTime(2000),
							UpdatedAt: mock.TestTime(2000),
						},
						Username: "JohnDoe",
					}, nil
				},
			},
			wantStatus: http.StatusOK,
			wantResp: &model.User{
				Base: model.Base{
					ID:        1,
					CreatedAt: mock.TestTime(2000),
					UpdatedAt: mock.TestTime(2000),
				},
				Username: "JohnDoe",
			},
			jwt: &mock.JWT{
				GenerateTokenFn: func(*model.User) (string, string, error) {
					return "jwttokenstring", mock.TestTime(2018).Format(time.RFC3339), nil
				},
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			r := server.New("../../../")
			rg := r.Group("/v1/users")
			service.NewUser(user.New(nil, tt.udb, tt.rbac, tt.auth), auth.New(nil, tt.udb, tt.jwt), rg, nil)
			ts := httptest.NewServer(r)
			defer ts.Close()
			path := ts.URL + "/v1/users/" + tt.req
			res, err := http.Get(path)
			if err != nil {
				t.Fatal(err)
			}
			defer res.Body.Close()
			if tt.wantResp != nil {
				response := new(model.User)
				if err := json.NewDecoder(res.Body).Decode(response); err != nil {
					t.Fatal(err)
				}
				assert.Equal(t, tt.wantResp, response)
			}
			assert.Equal(t, tt.wantStatus, res.StatusCode)
		})
	}
}

func TestUpdateUser(t *testing.T) {
	cases := []struct {
		name       string
		req        string
		id         string
		wantStatus int
		wantResp   *model.User
		udb        *mockdb.User
		rbac       *mock.RBAC
		auth       *mock.Auth
		jwt        *mock.JWT
	}{
		{
			name:       "Invalid request",
			id:         `a`,
			wantStatus: http.StatusMethodNotAllowed,
			jwt: &mock.JWT{
				GenerateTokenFn: func(*model.User) (string, string, error) {
					return "jwttokenstring", mock.TestTime(2018).Format(time.RFC3339), nil
				},
			},
		},
		{
			name: "Fail on RBAC",
			id:   `1`,
			req:  `{"first_name":"jj","last_name":"okocha","mobile":"123456","phone":"321321","address":"home"}`,
			rbac: &mock.RBAC{
				EnforceUserFn: func(echo.Context, int) error {
					return echo.ErrForbidden
				},
			},
			wantStatus: http.StatusMethodNotAllowed,
			jwt: &mock.JWT{
				GenerateTokenFn: func(*model.User) (string, string, error) {
					return "jwttokenstring", mock.TestTime(2018).Format(time.RFC3339), nil
				},
			},
		},
		{
			name: "Success",
			id:   `1`,
			req:  `{"first_name":"jj","last_name":"okocha","phone":"321321","address":"home"}`,
			rbac: &mock.RBAC{
				EnforceUserFn: func(echo.Context, int) error {
					return nil
				},
			},
			udb: &mockdb.User{
				ViewFn: func(db orm.DB, id int) (*model.User, error) {
					return &model.User{
						Base: model.Base{
							ID:        1,
							CreatedAt: mock.TestTime(2000),
							UpdatedAt: mock.TestTime(2000),
						},
						Username: "JohnDoe",
					}, nil
				},
				UpdateFn: func(db orm.DB, usr *model.User) (*model.User, error) {
					usr.UpdatedAt = mock.TestTime(2010)
					return usr, nil
				},
			},
			wantStatus: http.StatusOK,
			wantResp: &model.User{
				Base: model.Base{
					ID:        1,
					CreatedAt: mock.TestTime(2000),
					UpdatedAt: mock.TestTime(2010),
				},
				Username: "JohnDoe",
			},
			jwt: &mock.JWT{
				GenerateTokenFn: func(*model.User) (string, string, error) {
					return "jwttokenstring", mock.TestTime(2018).Format(time.RFC3339), nil
				},
			},
		},
	}

	client := http.Client{}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			r := server.New("../../../")
			rg := r.Group("/v1/users")
			service.NewUser(user.New(nil, tt.udb, tt.rbac, tt.auth), auth.New(nil, tt.udb, tt.jwt), rg, nil)
			ts := httptest.NewServer(r)
			defer ts.Close()
			path := ts.URL + "/v1/users/" + tt.id
			req, _ := http.NewRequest("PATCH", path, bytes.NewBufferString(tt.req))
			req.Header.Set("Content-Type", "application/json")
			res, err := client.Do(req)
			if err != nil {
				t.Fatal(err)
			}
			defer res.Body.Close()
			if tt.wantResp != nil {
				response := new(model.User)
				if err := json.NewDecoder(res.Body).Decode(response); err != nil {
					t.Fatal(err)
				}
				assert.Equal(t, tt.wantResp, response)
			}
			assert.Equal(t, tt.wantStatus, res.StatusCode)
		})
	}
}
