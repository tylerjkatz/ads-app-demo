package service

import (
	"fmt"
	"github.com/labstack/gommon/log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/labstack/echo"

	"gitlab.com/tylerjkatz/ads-app-demo/internal"

	"gitlab.com/tylerjkatz/ads-app-demo/internal/auth"

	"gitlab.com/tylerjkatz/ads-app-demo/internal/user"

	"gitlab.com/tylerjkatz/ads-app-demo/cmd/api/request"
)

const TMP_DIR_NAME string = "tmp"

// NewUser creates new user http service
func NewUser(svc *user.Service, asvc *auth.Service, ur *echo.Group, mw echo.MiddlewareFunc) {
	u := User{svc: svc, asvc: asvc}

	//get list of users authorized to be viewed by current user (normally 1 but all users if role is SUPER_ADMIN)
	ur.GET("/", u.list)

	//get single user by ID if authorized
	ur.GET("/:id/", u.view)

	//get user image
	ur.GET("/:id/image/:imgName/", u.getUserImage)

	//get campaigns of user with ID if authorized
	ur.GET("/:id/campaigns/", u.listCampaigns)

	//navigate to user home
	ur.GET("/home/", u.home)

	// swagger:route GET /v1/users/me auth meReq
	//retrieve currently logged-in user
	// Gets user's info from session
	// responses:
	//  200: userResp
	//  500: err
	ur.GET("/me/", u.me, mw)

	// swagger:route POST /logout auth logout
	// description: Logs user out immediately.
	// parameters:
	// - name: token
	//   in: path
	//   description: refresh token
	//   type: string
	//   required: true
	// responses:
	//  200: logoutResp
	//  400: errMsg
	//  401: errMsg
	// 	403: err
	//  404: errMsg
	//  500: err
	ur.POST("/logout/", u.logout, mw)

	// swagger:operation GET /refresh/{token} auth refresh
	// ---
	// summary: Refreshes jwt token.
	// description: If token passes check against db, generate/return new token with new expiration.
	// parameters:
	// - name: token
	//   in: path
	//   description: refresh token
	//   type: string
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/refreshResp"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.GET("/refresh/:token/", u.refresh, mw)
}

// User represents user http service
type User struct {
	svc  *user.Service
	asvc *auth.Service
}

func (u *User) getUserImage(c echo.Context) error {
	id, err := request.ID(c)
	if err != nil {
		code := http.StatusBadRequest
		return echo.NewHTTPError(code, map[string]string{
			"message": http.StatusText(code),
		})
	}

	imgName := request.ImageName(c)

	result, err := u.svc.GetUserImage(c, id, imgName)
	if err != nil {
		return err
	}

	//create temp file name for retrieved image in format "<time_retrieved>_<userID>_<imageName>"
	fileName := filepath.Join(TMP_DIR_NAME, fmt.Sprintf("%s_%d_%s",
		strings.Replace(time.Now().Format(time.RFC3339), ":", ".", -1), id, imgName))

	//convert result JpegImage to file with given name
	err = result.JpegImageToFile(fileName)
	if err != nil {
		code := http.StatusInternalServerError
		return echo.NewHTTPError(code, map[string]string{
			"message": http.StatusText(code),
		})
	}

	//delete file in 10 seconds to keep temp folder clean
	go removeFileAfterTimer(time.NewTimer(time.Second*10), fileName)

	return c.File(fileName)
}

//helper for above getUserImage() func
func removeFileAfterTimer(timer *time.Timer, fileName string) {
	<-timer.C
	err := os.Remove(fileName)
	if err != nil {
		log.Printf("ERROR: could not remove file %s", fileName)
	}
}

func (u *User) home(c echo.Context) error {
	user, err := u.asvc.Me(c)
	if err != nil {
		return c.Redirect(http.StatusTemporaryRedirect, "/unauth.html")
	}
	return c.Render(http.StatusOK, "userhome.html", map[string]interface{}{
		"Username":   user.Username,
		"DateJoined": user.CreatedAt.Format("Jan 2006"),
	})
}

func (u *User) list(c echo.Context) error {
	p, err := request.Paginate(c)
	if err != nil {
		code := http.StatusBadRequest
		return echo.NewHTTPError(code, map[string]string{
			"message": http.StatusText(code),
		})
	}
	result, err := u.svc.List(c, &model.Pagination{
		Limit: p.Limit, Offset: p.Offset,
	})
	if err != nil {
		code := http.StatusNotFound
		return echo.NewHTTPError(code, map[string]string{
			"message": http.StatusText(code),
		})
	}
	return c.JSON(http.StatusOK, listResponse{result, p.Page})
}

func (u *User) listCampaigns(c echo.Context) error {
	id, err := request.ID(c)
	if err != nil {
		code := http.StatusBadRequest
		return echo.NewHTTPError(code, map[string]string{
			"message": http.StatusText(code),
		})
	}

	result, err := u.svc.ListCampaigns(c, id)
	if err != nil || result == nil {
		code := http.StatusNotFound
		return echo.NewHTTPError(code, map[string]string{
			"message": http.StatusText(code),
		})
	}

	return c.JSON(http.StatusOK, result)
}

func (u *User) logout(c echo.Context) error {
	//set cookie to empty value and current expiration
	c.SetCookie(&http.Cookie{
		Name:    "ads_app_demo_auth_token",
		Value:   "",
		Expires: time.Now(),
		Path:    "/",
	})

	err := u.asvc.Logout(c)
	if err != nil {
		return c.Redirect(http.StatusTemporaryRedirect, "/unauth.html")
	}
	return c.NoContent(http.StatusOK)
}

func (u *User) me(c echo.Context) error {
	user, err := u.asvc.Me(c)
	if err != nil {
		return c.Redirect(http.StatusTemporaryRedirect, "/unauth.html")
	}
	return c.JSON(http.StatusOK, user)
}

func (u *User) view(c echo.Context) error {
	id, err := request.ID(c)
	if err != nil {
		code := http.StatusBadRequest
		return echo.NewHTTPError(code, map[string]string{
			"message": http.StatusText(code),
		})
	}

	result, err := u.svc.View(c, id)
	if err != nil || result == nil {
		code := http.StatusNotFound
		return echo.NewHTTPError(code, map[string]string{
			"message": http.StatusText(code),
		})
	}

	return c.JSON(http.StatusOK, result)
}

func (u *User) refresh(c echo.Context) error {
	r, err := u.asvc.Refresh(c, c.Param("token"))
	if err != nil {
		return c.JSON(http.StatusUnauthorized, map[string]string{
			"message": "invalid refresh credentials",
		})
	}

	//convert expiration to time.Time
	timeExp, _ := time.Parse(time.RFC3339, r.Expires)

	//set jwt cookie needed in header for auth requests
	c.SetCookie(&http.Cookie{
		Name:    "ads_app_demo_auth_token",
		Value:   r.Token,
		Expires: timeExp,
		Path:    "/",
	})

	return c.JSON(http.StatusOK, r)
}

type listResponse struct {
	Users []model.User `json:"users"`
	Page  int          `json:"page"`
}
