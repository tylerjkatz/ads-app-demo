package request

import (
	"github.com/labstack/echo"
	"strings"
)

// Credentials contains login request
//swagger:model
type Credentials struct {
	Username string `json:"username" form:"uname" validate:"required"`
	Password string `json:"password" form:"pw" validate:"required"`
}

// Login validates login request
func Login(c echo.Context) (*Credentials, error) {
	cred := new(Credentials)
	if err := c.Bind(cred); err != nil {
		return nil, err
	}

	//Username to lowercase and trim
	cred.Username = strings.TrimSpace(strings.ToLower(cred.Username))

	return cred, nil
}
