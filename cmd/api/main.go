package main

import (
	"net/http"
	"os"

	"github.com/go-pg/pg"
	"github.com/labstack/echo"
	"gitlab.com/tylerjkatz/ads-app-demo/cmd/api/config"
	"gitlab.com/tylerjkatz/ads-app-demo/cmd/api/mw"
	"gitlab.com/tylerjkatz/ads-app-demo/cmd/api/server"
	"gitlab.com/tylerjkatz/ads-app-demo/cmd/api/service"
	_ "gitlab.com/tylerjkatz/ads-app-demo/cmd/api/swagger"
	"gitlab.com/tylerjkatz/ads-app-demo/internal/auth"
	"gitlab.com/tylerjkatz/ads-app-demo/internal/platforms/postgres"
	"gitlab.com/tylerjkatz/ads-app-demo/internal/rbac"
	"gitlab.com/tylerjkatz/ads-app-demo/internal/user"
)

func main() {
	//get config env
	env := os.Getenv("ECHO_ENV")

	//default env to "dev"
	if env == "" {
		env = "dev"
	}

	//load config data from yaml file
	cfg, err := config.Load(env)
	checkErr(err)

	//create temporary folder
	os.Mkdir(service.TMP_DIR_NAME, os.ModePerm)

	e := server.New("")

	//serve swaggerUI if env is "dev"
	if env == "dev" {
		e.Static("/swaggerui", "cmd/api/swaggerui")
	}

	//configure db and create schema if cfg.DB.CreateSchema == true
	db, err := pgsql.New(cfg.DB)
	checkErr(err)

	//init v1 api services
	addV1Services(cfg, e, db)

	//start server/api
	server.Start(e, cfg.Server)
}

//addv2Services initializes services, routing and domains for api
func addV1Services(cfg *config.Configuration, e *echo.Echo, db *pg.DB) {
	//init DB interfaces
	userDB := pgsql.NewUserDB(e.Logger)

	//init JWT middleware
	jwt := mw.NewJWT(cfg.JWT)
	authSvc := auth.New(db, userDB, jwt)
	service.NewAuth(authSvc, e, jwt.MWFunc())

	//init auth service
	rbacSvc := rbac.New(userDB)

	//create router group for api v1
	v1Router := e.Group("/v1")
	v1Router.Any("/*", func(c echo.Context) error {
		code := http.StatusMethodNotAllowed
		return echo.NewHTTPError(code, map[string]string{
			"message": http.StatusText(code),
		})
	})

	//use jwt middleware for v1 api
	v1Router.Use(jwt.MWFunc())

	//create "users" router group within v1 api
	uR := v1Router.Group("/users")
	uR.Any("/*", func(c echo.Context) error {
		code := http.StatusMethodNotAllowed
		return echo.NewHTTPError(code, map[string]string{
			"message": http.StatusText(code),
		})
	})

	//create data model services
	service.NewUser(user.New(db, userDB, rbacSvc, authSvc), authSvc, uR, jwt.MWFunc())
}

//checkErr util func
func checkErr(err error) {
	if err != nil {
		panic(err.Error())
	}
}
