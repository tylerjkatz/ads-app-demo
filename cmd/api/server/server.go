package server

import (
	"context"
	"github.com/labstack/gommon/log"
	"html/template"
	"io"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/middleware"
	"gitlab.com/tylerjkatz/ads-app-demo/cmd/api/config"
	"gitlab.com/tylerjkatz/ads-app-demo/cmd/api/mw"

	"github.com/labstack/echo"
)

// New instantates new Echo server
func New(staticPathPrefix string) *echo.Echo {
	e := echo.New()
	e.Logger.SetLevel(log.INFO)
	e.Pre(middleware.AddTrailingSlash())

	e.File("/favicon.ico", staticPathPrefix+"assets/public/img/favicon.ico")
	e.Static("/", staticPathPrefix+"assets/public/html")
	e.Static("/js", staticPathPrefix+"assets/public/js")
	e.Static("/css", staticPathPrefix+"assets/public/css")
	e.Static("/img", staticPathPrefix+"assets/public/img")

	//init html template renderer
	e.Renderer = &TemplateRenderer{
		templates: template.Must(template.ParseGlob(staticPathPrefix + "assets/views/*.html")),
	}

	//status endpoint
	e.GET("/appOK", func(c echo.Context) error {
		return c.JSON(http.StatusOK, "OK")
	})

	//add middleware
	mw.Add(e, middleware.Logger(), middleware.Recover(),
		mw.CORS(), mw.SecureHeaders())
	e.Validator = &CustomValidator{V: validator.New()}
	custErr := &customErrHandler{e: e}
	e.HTTPErrorHandler = custErr.handler
	e.Binder = &CustomBinder{}
	return e
}

// Start starts echo server
func Start(e *echo.Echo, cfg *config.Server) {
	e.Server.Addr = cfg.Port
	e.Server.ReadTimeout = time.Duration(cfg.ReadTimeout) * time.Minute
	e.Server.WriteTimeout = time.Duration(cfg.WriteTimeout) * time.Minute
	e.Debug = cfg.Debug

	// Start server
	go func() {
		if err := e.Start(cfg.Port); err != nil {
			e.Logger.Info("shutting down the server")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 10 seconds.
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
}

type TemplateRenderer struct {
	templates *template.Template
}

func (t *TemplateRenderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}
