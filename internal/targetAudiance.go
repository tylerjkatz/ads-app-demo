package model

import (
	"github.com/go-pg/pg/orm"
)

// TargetAudiance represents targetAudiance domain model
type TargetAudiance struct {
	BaseNoID

	ID int `json:"-"`

	LanguageEnums []Language `json:"-" pg:",array"` //Enum

	Languages []string `json:"languages,omitempty" sql:"-"`

	GenderEnums []Gender `json:"-" pg:",array"` //Enum

	Genders []string `json:"genders,omitempty" sql:"-"`

	AgeRange []int `json:"age_range,omitempty" pg:",array"`

	LocationEnums []Location `json:"-" pg:",array"` //Enum

	Locations []string `json:"locations,omitempty" sql:"-"`

	Interests []string `json:"interests,omitempty" pg:",array"`

	KeyWords []string `json:"KeyWords,omitempty" pg:",array"`
}

// EnumsToStrings writes the int values of TargetAudiance enums to their string counterparts
func (ta *TargetAudiance) EnumsToStrings() error {
	for _, gender := range ta.GenderEnums {
		ta.Genders = append(ta.Genders, gender.String())
	}

	for _, language := range ta.LanguageEnums {
		ta.Languages = append(ta.Languages, language.String())
	}

	for _, location := range ta.LocationEnums {
		ta.Locations = append(ta.Locations, location.String())
	}

	return nil
}

// StringsToEnums writes the string values of TargetAudiance enum string slices to their int counterparts
func (ta *TargetAudiance) StringsToEnums() error {
	for _, gender := range ta.Genders {
		ta.GenderEnums = append(ta.GenderEnums, GenderToEnum(gender))
	}

	for _, language := range ta.Languages {
		ta.LanguageEnums = append(ta.LanguageEnums, LanguageToEnum(language))
	}

	for _, location := range ta.Locations {
		ta.LocationEnums = append(ta.LocationEnums, LocationToEnum(location))
	}

	return nil
}

// TargetAudianceDB represents targetAudiance database interface (repository)
type TargetAudianceDB interface {
	FindByID(orm.DB, int) (*TargetAudiance, error)
	List(orm.DB, *ListQuery, *Pagination) ([]TargetAudiance, error)
	Delete(orm.DB, *TargetAudiance) error
	Update(orm.DB, *TargetAudiance) (*TargetAudiance, error)
}
