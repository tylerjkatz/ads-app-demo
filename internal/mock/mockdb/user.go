package mockdb

import (
	"github.com/go-pg/pg/orm"
	"gitlab.com/tylerjkatz/ads-app-demo/internal"
)

// User database mock
type User struct {
	ViewFn           func(orm.DB, int) (*model.User, error)
	FindByUsernameFn func(orm.DB, string) (*model.User, error)
	FindByTokenFn    func(orm.DB, string) (*model.User, error)
	ListFn           func(orm.DB, *model.ListQuery, *model.Pagination) ([]model.User, error)
	ListCampaignsFn  func(orm.DB, int) ([]model.Campaign, error)
	GetUserImageFn   func(orm.DB, int, string) (*model.UserImage, error)
	UpdateFn         func(db orm.DB, user *model.User) (*model.User, error)
}

// View mock
func (u *User) View(db orm.DB, id int) (*model.User, error) {
	return u.ViewFn(db, id)
}

// FindByUsername mock
func (u *User) FindByUsername(db orm.DB, username string) (*model.User, error) {
	return u.FindByUsernameFn(db, username)
}

// FindByToken mock
func (u *User) FindByToken(db orm.DB, token string) (*model.User, error) {
	return u.FindByTokenFn(db, token)
}

// GetUserImage mock
func (u *User) GetUserImage(db orm.DB, id int, imgName string) (*model.UserImage, error) {
	return u.GetUserImage(db, id, imgName)
}

// List mock
func (u *User) List(db orm.DB, lq *model.ListQuery, p *model.Pagination) ([]model.User, error) {
	return u.ListFn(db, lq, p)
}

//
func (u *User) ListCampaigns(db orm.DB, id int) ([]model.Campaign, error) {
	return u.ListCampaignsFn(db, id)
}

// Update mock
func (u *User) Update(db orm.DB, usr *model.User) (*model.User, error) {
	return u.UpdateFn(db, usr)
}
