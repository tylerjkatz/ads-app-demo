package mock

import (
	"github.com/labstack/echo"

	"gitlab.com/tylerjkatz/ads-app-demo/internal"
)

// RBAC Mock
type RBAC struct {
	EnforceRoleFn   func(echo.Context, model.AccessRole) error
	EnforceUserFn   func(echo.Context, int) error
	AccountCreateFn func(echo.Context, int) error
	IsLowerRoleFn   func(echo.Context, model.AccessRole) error
}

// EnforceRole mock
func (a *RBAC) EnforceRole(c echo.Context, role model.AccessRole) error {
	return a.EnforceRoleFn(c, role)
}

// EnforceUser mock
func (a *RBAC) EnforceUser(c echo.Context, id int) error {
	return a.EnforceUserFn(c, id)
}

// AccountCreate mock
func (a *RBAC) AccountCreate(c echo.Context, roleID int) error {
	return a.AccountCreateFn(c, roleID)
}

// IsLowerRole mock
func (a *RBAC) IsLowerRole(c echo.Context, role model.AccessRole) error {
	return a.IsLowerRoleFn(c, role)
}
