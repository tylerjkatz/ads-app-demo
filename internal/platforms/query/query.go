package query

import (
	"github.com/labstack/echo"
	"gitlab.com/tylerjkatz/ads-app-demo/internal"
)

// List prepares prepares admin list query if sufficient access
func List(u *model.AuthUser) (*model.ListQuery, error) {
	switch true {
	case int(u.Role) <= 2: // user is SuperAdmin or Admin
		return nil, nil
	default:
		return nil, echo.ErrForbidden
	}
}
