package pgsql

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"

	// DB adapter
	_ "github.com/lib/pq"
	"gitlab.com/tylerjkatz/ads-app-demo/internal"
	"gitlab.com/tylerjkatz/ads-app-demo/internal/auth"

	"gitlab.com/tylerjkatz/ads-app-demo/cmd/api/config"
)

const INPUT_SOURCE_DIR = "all_init_data"
const NOT_DELETED = "deleted_at is null"
const ADMIN_USER_ID = 1

// New creates new database connection to a postgres database
// Drops existing schema and recreates from data.json
func New(cfg *config.Database) (*pg.DB, error) {
	opts, err := pg.ParseURL(cfg.PSN)
	if err != nil {
		return nil, err
	}

	//lower connection pool size from default of 10 (set by vendor)
	opts.PoolSize = 2

	db := pg.Connect(opts)
	_, err = db.Exec("SELECT 1")
	if err != nil {
		return nil, err
	}
	if cfg.Timeout > 0 {
		db.WithTimeout(time.Second * time.Duration(cfg.Timeout))
	}
	if cfg.Log {
		db.OnQueryProcessed(func(event *pg.QueryProcessedEvent) {
			query, err := event.FormattedQuery()
			checkErr(err)
			log.Printf("%s | %s", time.Since(event.StartTime), query)
		})
	}

	//create db schema
	if cfg.CreateSchema {
		//First, wipe current schema if exists
		_, err = db.Exec(`DROP TABLE IF EXISTS users, user_images, campaigns, creatives, insights, 
			platforms, roles, target_audiances CASCADE`)
		checkErr(err)

		//Create schema
		createSchema(db, &model.TargetAudiance{}, &model.Creatives{}, &model.Insights{},
			&model.Role{}, &model.User{}, &model.UserImage{}, &model.Campaign{}, &model.Platform{},
		)

		//add admin user
		insertAdminUser(db)

		//add user images
		insertUserImages(db)

		//add campaign data from data.json file
		insertUserCampaigns(db)

	}

	return db, nil
}

func insertAdminUser(db *pg.DB) {
	db.Insert(&model.Role{
		ID:          ADMIN_USER_ID,
		AccessLevel: model.SuperAdminRole,
		Name:        model.SuperAdminRole.String(),
	})

	//add admin user
	db.Insert(&model.User{
		Base: model.Base{
			ID:        ADMIN_USER_ID,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
			DeletedAt: nil,
		},
		Username:  "admin",
		Password:  auth.HashPassword("nanos"),
		LastLogin: nil,
		Active:    true,
		Token:     "1",
		RoleID:    1,
	})
}

func insertUserImages(db *pg.DB) {
	var userImage *model.UserImage
	for i := 1; i <= 4; i++ {
		userImage = &model.UserImage{
			UserID: ADMIN_USER_ID,
			Name:   fmt.Sprintf("img%d.jpg", i),
		}
		err := userImage.FileToJpegBytes(filepath.Join(".", INPUT_SOURCE_DIR, "images", userImage.Name))
		if err != nil {
			log.Println(err)
			continue
		}
		db.Insert(userImage)
	}
}

func insertUserCampaigns(db *pg.DB) {
	//read data.json into slice of campaigns

	//open file
	dataJSON, err := os.Open(filepath.Join(".", INPUT_SOURCE_DIR, "data", "data.json"))
	checkErr(err)

	//read file
	bytes, err := ioutil.ReadAll(dataJSON)

	//close file
	dataJSON.Close()
	checkErr(err)

	//unmarshal
	var campaigns []*model.Campaign
	err = json.Unmarshal(bytes, &campaigns)
	checkErr(err)

	//DEBUG - print campaigns as JSON
	/*
		bytesJSON, err := json.MarshalIndent(campaigns, "", "  ")
		log.Printf("campaigns:\n%v\n", string(bytesJSON))
	*/

	//prepare and insert campaigns
	var targetAudianceID, creativesID, insightsID, platformID int
	for _, campaign := range campaigns {
		//copy PlatformMap to Platforms list for this campaign
		err = campaign.PlatformMapToList()
		checkErr(err)

		for _, platform := range campaign.Platforms {
			if platform != nil && platform.TargetAudiance != nil {
				//copy JSON string values to enum ints for the 3 TargetAudience enums
				err := platform.TargetAudiance.StringsToEnums()
				checkErr(err)

				//record timestamps
				now := time.Now()

				//add TargetAudiance
				targetAudianceID++
				platform.TargetAudiance.ID = targetAudianceID
				platform.TargetAudiance.CreatedAt = now
				platform.TargetAudiance.UpdatedAt = now
				db.Insert(platform.TargetAudiance)

				//add Creatives
				creativesID++
				if platform.Creatives == nil {
					platform.Creatives = &model.Creatives{}
				}
				platform.Creatives.ID = creativesID
				platform.Creatives.CreatedAt = now
				platform.Creatives.UpdatedAt = now
				db.Insert(platform.Creatives)

				//add Insights
				insightsID++
				if platform.Insights == nil {
					platform.Insights = &model.Insights{}
				}
				platform.Insights.ID = insightsID
				platform.Insights.CreatedAt = now
				platform.Insights.UpdatedAt = now
				db.Insert(platform.Insights)

				//add platform
				platformID++
				platform.CampaignID = campaign.ID
				platform.TargetAudianceID = targetAudianceID
				platform.InsightsID = insightsID
				platform.CreativesID = creativesID
				platform.CreatedAt = now
				platform.UpdatedAt = now
				db.Insert(platform)
			}
		}

		//add campaign
		campaign.UserID = ADMIN_USER_ID
		db.Insert(campaign)
	}
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func createSchema(db *pg.DB, models ...interface{}) {
	for _, model := range models {
		checkErr(db.CreateTable(model, &orm.CreateTableOptions{
			FKConstraints: true,
		}))
	}
}
