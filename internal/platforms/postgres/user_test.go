package pgsql_test

import (
	"testing"

	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tylerjkatz/ads-app-demo/internal/platforms/postgres"

	"github.com/go-pg/pg"
	"gitlab.com/tylerjkatz/ads-app-demo/internal"
)

func testUserDB(t *testing.T, c *pg.DB, l echo.Logger) {
	userDB := pgsql.NewUserDB(l)
	cases := []struct {
		name string
		fn   func(*testing.T, *pgsql.UserDB, *pg.DB)
	}{
		{
			name: "view",
			fn:   testUserView,
		},
		{
			name: "findByUsername",
			fn:   testUserFindByUsername,
		},
		{
			name: "findByToken",
			fn:   testUserFindByToken,
		},
		{
			name: "userList",
			fn:   testUserList,
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			tt.fn(t, userDB, c)
		})
	}

}

func testUserView(t *testing.T, db *pgsql.UserDB, c *pg.DB) {
	cases := []struct {
		name     string
		wantErr  bool
		id       int
		wantData *model.User
	}{
		{
			name:    "User does not exist",
			wantErr: true,
			id:      1000,
		},
		{
			name: "Success",
			id:   2,
			wantData: &model.User{
				Username: "tomjones",
				RoleID:   1,
				Password: "newPass",
				Base: model.Base{
					ID: 2,
				},
				Role: &model.Role{
					ID:          1,
					AccessLevel: 1,
					Name:        "SUPER_ADMIN",
				},
			},
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			user, err := db.View(c, tt.id)
			assert.Equal(t, tt.wantErr, err != nil)
			if tt.wantData != nil {
				tt.wantData.CreatedAt = user.CreatedAt
				tt.wantData.UpdatedAt = user.UpdatedAt
				assert.Equal(t, tt.wantData, user)
			}
		})
	}
}

func testUserFindByUsername(t *testing.T, db *pgsql.UserDB, c *pg.DB) {
	cases := []struct {
		name     string
		wantErr  bool
		username string
		wantData *model.User
	}{
		{
			name:     "User does not exist",
			wantErr:  true,
			username: "notExists",
		},
		{
			name:     "Success",
			username: "tomjones",
			wantData: &model.User{
				Username: "tomjones",
				RoleID:   1,
				Password: "newPass",
				Base: model.Base{
					ID: 2,
				},
				Role: &model.Role{
					ID:          1,
					AccessLevel: 1,
					Name:        "SUPER_ADMIN",
				},
			},
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			user, err := db.FindByUsername(c, tt.username)
			assert.Equal(t, tt.wantErr, err != nil)

			if tt.wantData != nil {
				tt.wantData.CreatedAt = user.CreatedAt
				tt.wantData.UpdatedAt = user.UpdatedAt
				assert.Equal(t, tt.wantData, user)

			}
		})
	}
}

func testUserFindByToken(t *testing.T, db *pgsql.UserDB, c *pg.DB) {
	cases := []struct {
		name     string
		wantErr  bool
		token    string
		wantData *model.User
	}{
		{
			name:    "User does not exist",
			wantErr: true,
			token:   "notExists",
		},
		{
			name:  "Success",
			token: "loginrefresh",
			wantData: &model.User{
				Username: "johndoe",
				RoleID:   1,
				Password: "hunter2",
				Base: model.Base{
					ID: 1,
				},
				Role: &model.Role{
					ID:          1,
					AccessLevel: 1,
					Name:        "SUPER_ADMIN",
				},
				Token: "loginrefresh",
			},
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			user, err := db.FindByToken(c, tt.token)
			assert.Equal(t, tt.wantErr, err != nil)

			if tt.wantData != nil {
				tt.wantData.CreatedAt = user.CreatedAt
				tt.wantData.UpdatedAt = user.UpdatedAt
				assert.Equal(t, tt.wantData, user)

			}
		})
	}
}

func testUserList(t *testing.T, db *pgsql.UserDB, c *pg.DB) {
	cases := []struct {
		name     string
		wantErr  bool
		qp       *model.ListQuery
		pg       *model.Pagination
		wantData []model.User
	}{
		{
			name:    "Invalid pagination values",
			wantErr: true,
			pg: &model.Pagination{
				Limit: -100,
			},
		},
		{
			name: "Success",
			pg: &model.Pagination{
				Limit:  100,
				Offset: 0,
			},
			qp: &model.ListQuery{
				ID:    1,
				Query: "company_id = ?",
			},
			wantData: []model.User{
				{
					Username: "tomjones",
					RoleID:   1,
					Password: "newPass",
					Base: model.Base{
						ID: 2,
					},
					Role: &model.Role{
						ID:          1,
						AccessLevel: 1,
						Name:        "SUPER_ADMIN",
					},
				},
				{
					Username: "johndoe",
					RoleID:   1,
					Password: "hunter2",
					Base: model.Base{
						ID: 1,
					},
					Role: &model.Role{
						ID:          1,
						AccessLevel: 1,
						Name:        "SUPER_ADMIN",
					},
					Token: "loginrefresh",
				},
			},
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			users, err := db.List(c, tt.qp, tt.pg)
			assert.Equal(t, tt.wantErr, err != nil)
			if tt.wantData != nil {
				for i, v := range users {
					tt.wantData[i].CreatedAt = v.CreatedAt
					tt.wantData[i].UpdatedAt = v.UpdatedAt
				}
				assert.Equal(t, tt.wantData, users)
			}
		})
	}
}
