package pgsql

import (
	"github.com/go-pg/pg/orm"
	"github.com/labstack/echo"
	"gitlab.com/tylerjkatz/ads-app-demo/internal"
)

// NewUserDB returns a new UserDB instance
func NewUserDB(l echo.Logger) *UserDB {
	return &UserDB{l}
}

// UserDB represents the client for user table
type UserDB struct {
	log echo.Logger
}

// View returns single user by ID
func (u *UserDB) View(db orm.DB, id int) (*model.User, error) {
	var user = new(model.User)
	err := db.Model(user).Relation("Role").Where(`"user"."id" = ?`, id).Select()
	if err != nil {
		u.log.Warnf("UserDB Error: %v", err)
		return nil, err
	}

	return user, nil
}

// List returns list of all users retrievable for the current user, depending on role
func (u *UserDB) List(db orm.DB, qp *model.ListQuery, p *model.Pagination) ([]model.User, error) {
	var users []model.User
	q := db.Model(&users).Column("user.*", "Role").Limit(p.Limit).Offset(p.Offset).Where(NOT_DELETED).Order("user.id desc")
	if qp != nil {
		q.Where(qp.Query, qp.ID)
	}
	if err := q.Select(); err != nil {
		u.log.Warnf("UserDB Error: %v", err)
		return nil, err
	}
	return users, nil
}

// ListCampaigns returns list of all campaigns associated with the user
func (u *UserDB) ListCampaigns(db orm.DB, id int) ([]model.Campaign, error) {
	var campaigns []model.Campaign
	q := db.Model(&campaigns).Column("campaign.*", "Platforms").Where("campaign.user_id = ?", id).Order("campaign.id")
	if err := q.Select(); err != nil {
		u.log.Warnf("UserDB Error: %v", err)
		return nil, err
	}
	for i, _ := range campaigns {
		campaigns[i].PlatformsToMap()
		for j, _ := range campaigns[i].Platforms {
			platform := campaigns[i].Platforms[j]

			//join TargetAudiance relation
			q := db.Model(platform).Relation("TargetAudiance").Where("platform.target_audiance_id = ?", platform.TargetAudianceID)
			if err := q.Select(); err != nil {
				u.log.Warnf("UserDB Error: %v", err)
				return nil, err
			}

			if err := platform.TargetAudiance.EnumsToStrings(); err != nil {
				u.log.Warnf("UserDB Error: %v", err)
				return nil, err
			}

			//join Creatives relation
			q = db.Model(platform).Relation("Creatives").Where("platform.creatives_id = ?", platform.CreativesID)
			if err := q.Select(); err != nil {
				u.log.Warnf("UserDB Error: %v", err)
				return nil, err
			}

			//join Insights relation
			q = db.Model(platform).Relation("Insights").Where("platform.insights_id = ?", platform.InsightsID)
			if err := q.Select(); err != nil {
				u.log.Warnf("UserDB Error: %v", err)
				return nil, err
			}
		}
	}
	return campaigns, nil
}

// FindByUsername queries for single user by username
func (u *UserDB) FindByUsername(db orm.DB, uname string) (*model.User, error) {
	var user = new(model.User)
	sql := `SELECT "user".*, "role"."id" AS "role__id", "role"."access_level" AS "role__access_level", "role"."name" AS "role__name" 
	FROM "users" AS "user" LEFT JOIN "roles" AS "role" ON "role"."id" = "user"."role_id" 
	WHERE ("user"."username" = ? and deleted_at is null)`
	_, err := db.QueryOne(user, sql, uname)
	if err != nil {
		u.log.Warnf("UserDB Error: %v", err)
		return nil, err
	}
	return user, nil
}

// FindByToken queries for single user by token
func (u *UserDB) FindByToken(db orm.DB, token string) (*model.User, error) {
	var user = new(model.User)
	sql := `SELECT "user".*, "role"."id" AS "role__id", "role"."access_level" AS "role__access_level", "role"."name" AS "role__name" 
	FROM "users" AS "user" LEFT JOIN "roles" AS "role" ON "role"."id" = "user"."role_id" 
	WHERE ("user"."token" = ? and deleted_at is null)`
	_, err := db.QueryOne(user, sql, token)
	if err != nil {
		u.log.Warnf("UserDB Error: %v", err)
		return nil, err
	}
	return user, nil
}

func (u *UserDB) GetUserImage(db orm.DB, id int, imgName string) (*model.UserImage, error) {
	//get UserImage from user_images where user_id and name match id and imgName args respectively
	var userImage = new(model.UserImage)
	err := db.Model(userImage).Where(`"user_image"."user_id" = ? and "user_image"."name" = ?`, id, imgName).Select()
	if err != nil {
		u.log.Warnf("UserDB Error: %v", err)
		return nil, err
	}

	return userImage, nil
}

// Update updates user's contact info
func (u *UserDB) Update(db orm.DB, user *model.User) (*model.User, error) {
	_, err := db.Model(user).WherePK().Update()
	if err != nil {
		u.log.Warnf("UserDB Error: %v", err)
		return nil, err
	}
	return user, nil
}
