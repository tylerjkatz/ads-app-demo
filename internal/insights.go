package model

import "time"

// Insights represents insights domain model
type Insights struct {
	ID        int        `json:"-"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-" pg:",soft_delete"`

	Impressions int `json:"impressions" sql:",notnull"`

	Clicks int `json:"clicks" sql:",notnull"`

	WebsiteVisits int `json:"website_visits" sql:",notnull"`

	NanosScore float64 `json:"nanos_score" sql:",notnull"`

	CostPerClick float64 `json:"cost_per_click" sql:",notnull"`

	ClickThroughRate float64 `json:"click_through_rate" sql:",notnull"`

	AdvancedKPI1 float64 `json:"advanced_kpi_1" sql:",notnull"`

	AdvancedKPI2 float64 `json:"advanced_kpi_2" sql:",notnull"`
}
