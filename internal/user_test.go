package model_test

import (
	"testing"

	"gitlab.com/tylerjkatz/ads-app-demo/internal"
)

func TestUpdateLastLogin(t *testing.T) {
	user := &model.User{
		Username: "TestGuy",
	}
	user.UpdateLastLogin()
	if user.LastLogin.IsZero() {
		t.Errorf("Last login time was not changed")
	}
}
