package model

import "time"

// Creatives represents creatives domain model
type Creatives struct {
	ID        int        `json:"-"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-" pg:",soft_delete"`

	Header string `json:"header,omitempty"`

	Header1 string `json:"header_1,omitempty"`

	Header2 string `json:"header_2,omitempty"`

	Description string `json:"description,omitempty"`

	URL string `json:"url,omitempty"`

	Image string `json:"image,omitempty"`
}
