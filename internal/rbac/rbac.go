package rbac

import (
	"github.com/labstack/echo"
	"gitlab.com/tylerjkatz/ads-app-demo/internal"
)

// New creates new RBAC service
func New(udb model.UserDB) *Service {
	return &Service{udb}
}

// Service is RBAC application service
type Service struct {
	udb model.UserDB
}

func checkBool(b bool) error {
	if b {
		return nil
	}
	return echo.ErrForbidden
}

// EnforceRole authorizes request by AccessRole
func (s *Service) EnforceRole(c echo.Context, r model.AccessRole) error {
	return checkBool(!(c.Get("role").(int8) > int8(r)))
}

// EnforceUser checks whether the request to change user data is done by the same user
func (s *Service) EnforceUser(c echo.Context, ID int) error {
	if s.isAdmin(c) {
		return nil
	}
	return checkBool(c.Get("id").(int) == ID)
}

func (s *Service) isAdmin(c echo.Context) bool {
	return !(c.Get("role").(int8) > int8(model.AdminRole))
}

// AccountCreate performs auth check when creating a new account
// Location admin cannot create accounts, needs to be fixed on EnforceLocation function
func (s *Service) AccountCreate(c echo.Context, roleID int) error {
	return s.IsLowerRole(c, model.AccessRole(roleID))
}

// IsLowerRole checks whether the requesting user has higher role than the user it wants to change
// Used for account creation/deletion
func (s *Service) IsLowerRole(c echo.Context, r model.AccessRole) error {
	return checkBool(c.Get("role").(int8) < int8(r))
}
