package model

// Platform represents platform domain model
type Platform struct {
	BaseNoID

	CampaignID int `json:"-" sql:",pk"`

	Name string `json:"-" sql:",pk"`

	Status string `json:"status,omitempty"`

	TotalBudget int `json:"total_budget" sql:",notnull"`

	RemainingBudget int `json:"remaining_budget" sql:",notnull"`

	StartDate int64 `json:"start_date,omitempty" sql:",notnull"`

	EndDate int64 `json:"end_date,omitempty" sql:",notnull"`

	TargetAudiance *TargetAudiance `json:"target_audiance,omitempty"`

	TargetAudianceID int `json:"-"`

	Creatives *Creatives `json:"creatives,omitempty"`

	CreativesID int `json:"-"`

	Insights *Insights `json:"insights,omitempty"`

	InsightsID int `json:"-"`
}
