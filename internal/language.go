package model

// Language represents language type
type Language int8

func LanguageToEnum(language string) Language {
	langs := map[string]Language{
		"DE": 1,
		"EN": 2,
		"FR": 3,
	}

	return langs[language]
}

func (lang Language) String() string {
	langs := [...]string{"DE", "EN", "FR"}

	return langs[lang-1]
}

const (
	// DE language
	LangDE Language = iota

	// EN language
	LangEN

	// FR language
	LangFR
)
