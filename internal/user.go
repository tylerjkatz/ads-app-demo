package model

import (
	"time"

	"github.com/go-pg/pg/orm"
)

// User represents user domain model
type User struct {
	Base

	Username  string     `json:"username" sql:",unique"`
	Password  string     `json:"-"`
	LastLogin *time.Time `json:"last_login,omitempty"`
	Active    bool       `json:"active"`
	Token     string     `json:"-"`

	Role *Role `json:"role,omitempty"`

	RoleID int `json:"-"`

	Campaigns []*Campaign `json:"campaigns,omitempty"`
}

// AuthUser represents data stored in JWT token for user
type AuthUser struct {
	ID       int
	Username string
	Role     AccessRole
}

// UpdateLastLogin updates last login field
func (u *User) UpdateLastLogin() {
	t := time.Now()
	u.LastLogin = &t
}

// UserDB represents user database interface (repository)
type UserDB interface {
	View(orm.DB, int) (*User, error)
	FindByUsername(orm.DB, string) (*User, error)
	FindByToken(orm.DB, string) (*User, error)
	GetUserImage(orm.DB, int, string) (*UserImage, error)
	List(orm.DB, *ListQuery, *Pagination) ([]User, error)
	ListCampaigns(orm.DB, int) ([]Campaign, error)
	Update(orm.DB, *User) (*User, error)
}
