package model

import (
	"bytes"
	"image"
	"image/jpeg"
	"log"
	"os"
)

// UserImage represents user image displayed with each platform
type UserImage struct {
	BaseNoID

	UserID int `json:"-" sql:",pk"`

	Name string `json:"name" sql:",pk"`

	JpegBytes []byte `json:"-"`

	JpegImage *image.Image `json:"-" sql:"-"`
}

// FileToJpegBytes writes static .jpg image file at given path to JpegBytes
func (ui *UserImage) FileToJpegBytes(path string) error {
	//open file at path
	imgFile, err := os.Open(path)
	if err != nil {
		log.Print(err)
		return err
	}

	//decode file to image object
	img, err := jpeg.Decode(imgFile)
	imgFile.Close()
	if err != nil {
		log.Print(err)
		return err
	}

	//encode image object to bytes buffer
	buff := new(bytes.Buffer)
	err = jpeg.Encode(buff, img, nil)
	if err != nil {
		log.Print(err)
		return err
	}

	//assign bytes to this JpegBytes
	ui.JpegBytes = buff.Bytes()

	return err
}

// JpegBytesToImage writes JpegBytes to JpegImage field
func (ui *UserImage) JpegBytesToImage() error {
	//create JpegBytes reader
	r := bytes.NewReader(ui.JpegBytes)

	//decode JpegBytes to image object
	img, err := jpeg.Decode(r)
	if err != nil {
		log.Print(err)
		return err
	}

	ui.JpegImage = &img

	return err
}

// JpegImageToFile writes JpegImage to a file with path/name 'filePath'
func (ui *UserImage) JpegImageToFile(filePath string) error {
	f, err := os.Create(filePath)
	if err != nil {
		return err
	}

	defer f.Close()

	err = jpeg.Encode(f, *ui.JpegImage, nil)

	return err
}
