package model

// Campaign represents user campaign model
type Campaign struct {
	Base

	Name string `json:"name"`

	Goal string `json:"goal"`

	TotalBudget int `json:"total_budget" sql:",notnull"`

	Status string `json:"status"`

	PlatformMap map[string]*Platform `json:"platforms,omitempty" sql:"-"`

	Platforms []*Platform `json:"-"`

	User *User `json:"-"`

	UserID int `json:"-"`
}

// PlatformMapToList converts PlatformMap to Platforms list (slice)
func (c *Campaign) PlatformMapToList() error {
	for name, platform := range c.PlatformMap {
		platform.Name = name
		c.Platforms = append(c.Platforms, platform)
	}

	return nil
}

// PlatformsToMap convertsPlatforms list (slice) to PlatformMap
func (c *Campaign) PlatformsToMap() error {
	for _, platform := range c.Platforms {
		if c.PlatformMap == nil {
			c.PlatformMap = map[string]*Platform{}
		}

		c.PlatformMap[platform.Name] = platform
	}

	return nil
}
