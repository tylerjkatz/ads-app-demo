package model

// Gender represents gender type
type Gender int8

func GenderToEnum(gender string) Gender {
	gends := map[string]Gender{
		"M": 1,
		"F": 2,
	}

	return gends[gender]
}

func (gend Gender) String() string {
	gends := [...]string{"M", "F"}

	return gends[gend-1]
}

const (
	// M Gender
	GendM Gender = iota

	// F Gender
	GendF
)
