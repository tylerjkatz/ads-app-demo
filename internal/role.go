package model

// AccessRole represents access role type
type AccessRole int8

func (ar AccessRole) String() string {
	roleNames := [...]string{"SUPER_ADMIN", "ADMIN", "USER"}

	return roleNames[ar-1]
}

const (
	// SuperAdminRole has all permissions
	SuperAdminRole AccessRole = iota + 1

	// AdminRole has admin specific permissions
	AdminRole

	// UserRole is a standard user
	UserRole
)

// Role model
type Role struct {
	ID          int        `json:"id"`
	AccessLevel AccessRole `json:"access_level"`
	Name        string     `json:"name"`
}
