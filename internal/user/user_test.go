package user_test

import (
	"testing"

	"github.com/go-pg/pg/orm"
	"github.com/labstack/echo"

	"github.com/stretchr/testify/assert"

	"gitlab.com/tylerjkatz/ads-app-demo/internal"
	"gitlab.com/tylerjkatz/ads-app-demo/internal/mock"
	"gitlab.com/tylerjkatz/ads-app-demo/internal/mock/mockdb"
	"gitlab.com/tylerjkatz/ads-app-demo/internal/user"
)

func TestView(t *testing.T) {
	type args struct {
		c  echo.Context
		id int
	}
	cases := []struct {
		name     string
		args     args
		wantData *model.User
		wantErr  error
		udb      *mockdb.User
		rbac     *mock.RBAC
	}{
		{
			name: "Fail on RBAC",
			args: args{id: 5},
			rbac: &mock.RBAC{
				EnforceUserFn: func(c echo.Context, id int) error {
					return model.ErrGeneric
				}},
			wantErr: model.ErrGeneric,
		},
		{
			name: "Success",
			args: args{id: 1},
			wantData: &model.User{
				Base: model.Base{
					ID:        1,
					CreatedAt: mock.TestTime(2000),
					UpdatedAt: mock.TestTime(2000),
				},
				Username: "JohnDoe",
			},
			rbac: &mock.RBAC{
				EnforceUserFn: func(c echo.Context, id int) error {
					return nil
				}},
			udb: &mockdb.User{
				ViewFn: func(db orm.DB, id int) (*model.User, error) {
					if id == 1 {
						return &model.User{
							Base: model.Base{
								ID:        1,
								CreatedAt: mock.TestTime(2000),
								UpdatedAt: mock.TestTime(2000),
							},
							Username: "JohnDoe",
						}, nil
					}
					return nil, nil
				}},
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			s := user.New(nil, tt.udb, tt.rbac, nil)
			usr, err := s.View(tt.args.c, tt.args.id)
			assert.Equal(t, tt.wantData, usr)
			assert.Equal(t, tt.wantErr, err)
		})
	}
}

func TestList(t *testing.T) {
	type args struct {
		c   echo.Context
		pgn *model.Pagination
	}
	cases := []struct {
		name     string
		args     args
		wantData []model.User
		wantErr  bool
		udb      *mockdb.User
		auth     *mock.Auth
	}{
		{
			name: "Fail on query List",
			args: args{c: nil, pgn: &model.Pagination{
				Limit:  100,
				Offset: 200,
			}},
			wantErr: true,
			auth: &mock.Auth{
				UserFn: func(c echo.Context) *model.AuthUser {
					return &model.AuthUser{
						ID:   1,
						Role: model.UserRole,
					}
				}}},
		{
			name: "Success",
			args: args{c: nil, pgn: &model.Pagination{
				Limit:  100,
				Offset: 200,
			}},
			auth: &mock.Auth{
				UserFn: func(c echo.Context) *model.AuthUser {
					return &model.AuthUser{
						ID:   1,
						Role: model.AdminRole,
					}
				}},
			udb: &mockdb.User{
				ListFn: func(orm.DB, *model.ListQuery, *model.Pagination) ([]model.User, error) {
					return []model.User{
						{
							Base: model.Base{
								ID:        1,
								CreatedAt: mock.TestTime(1999),
								UpdatedAt: mock.TestTime(2000),
							},
							Username: "johndoe",
						},
						{
							Base: model.Base{
								ID:        2,
								CreatedAt: mock.TestTime(2001),
								UpdatedAt: mock.TestTime(2002),
							},
							Username: "hunterlogan",
						},
					}, nil
				}},
			wantData: []model.User{
				{
					Base: model.Base{
						ID:        1,
						CreatedAt: mock.TestTime(1999),
						UpdatedAt: mock.TestTime(2000),
					},
					Username: "johndoe",
				},
				{
					Base: model.Base{
						ID:        2,
						CreatedAt: mock.TestTime(2001),
						UpdatedAt: mock.TestTime(2002),
					},
					Username: "hunterlogan",
				}},
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			s := user.New(nil, tt.udb, nil, tt.auth)
			usrs, err := s.List(tt.args.c, tt.args.pgn)
			assert.Equal(t, tt.wantData, usrs)
			assert.Equal(t, tt.wantErr, err != nil)
		})
	}
}
