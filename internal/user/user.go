// Package user contains user application services
package user

import (
	"github.com/go-pg/pg"
	"github.com/labstack/echo"
	"gitlab.com/tylerjkatz/ads-app-demo/internal"

	"gitlab.com/tylerjkatz/ads-app-demo/internal/platforms/query"
	//"gitlab.com/tylerjkatz/ads-app-demo/internal/platforms/structs"
)

// New creates new user application service
func New(db *pg.DB, udb model.UserDB, rbac model.RBACService, auth model.AuthService) *Service {
	return &Service{db: db, udb: udb, rbac: rbac, auth: auth}
}

// Service represents user application service
type Service struct {
	db   *pg.DB
	udb  model.UserDB
	rbac model.RBACService
	auth model.AuthService
}

// List returns list of users
func (s *Service) List(c echo.Context, p *model.Pagination) ([]model.User, error) {
	u := s.auth.User(c)
	q, err := query.List(u)
	if err != nil {
		return nil, err
	}
	results, err := s.udb.List(s.db, q, p)
	if err != nil {
		return nil, err
	}

	return results, nil
}

// View returns single user
func (s *Service) View(c echo.Context, id int) (*model.User, error) {
	var err error
	if err = s.rbac.EnforceUser(c, id); err != nil {
		return nil, err
	}

	result, err := s.udb.View(s.db, id)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// ListCampaignsByUser returns list of user's campaigns
func (s *Service) ListCampaigns(c echo.Context, id int) ([]model.Campaign, error) {
	var err error
	if err = s.rbac.EnforceUser(c, id); err != nil {
		return nil, err
	}

	result, err := s.udb.ListCampaigns(s.db, id)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// GetUserImage returns UserImage from db
func (s *Service) GetUserImage(c echo.Context, userID int, imgName string) (*model.UserImage, error) {
	var err error
	if err = s.rbac.EnforceUser(c, userID); err != nil {
		return nil, err
	}

	result, err := s.udb.GetUserImage(s.db, userID, imgName)
	if err != nil {
		return nil, err
	}

	//convert result JpegBytes to image.Image
	err = result.JpegBytesToImage()
	if err != nil {
		return nil, err
	}

	return result, nil
}
