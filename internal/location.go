package model

// Location represents location type
type Location int8

func LocationToEnum(location string) Location {
	locs := map[string]Location{
		"France":         1,
		"Germany":        2,
		"Switzerland":    3,
		"Italy":          4,
		"Spain":          5,
		"Belgium":        6,
		"United Kingdom": 7,
		"Poland":         8,
	}

	return locs[location]
}

func (loc Location) String() string {
	locs := [...]string{
		"France",
		"Germany",
		"Switzerland",
		"Italy",
		"Spain",
		"Belgium",
		"United Kingdom",
		"Poland",
	}

	return locs[loc-1]
}

const (
	// France Location
	LocFr Location = iota

	// Germany Location
	LocGer

	// Switzerland Location
	LocSwi

	// Italy Location
	LocIta

	// Spain Location
	LocSp

	// Belgium Location
	LocBel

	// United Kingdom Location
	LocUK

	// Poland Location
	LocPol
)
