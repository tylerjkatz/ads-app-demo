#!/usr/bin/env bash

# get deps
echo "Running 'dep ensure'..."
dep ensure
echo "Done!"

echo "Running tests..."
set -e
echo "" > coverage.txt

for d in $(cat pkglist | grep -v -e internal/mock -e cmd/api/server); do
    go test -race -coverprofile=profile.out -covermode=atomic "$d"
    if [ -f profile.out ]; then
        cat profile.out >> coverage.txt
        rm profile.out
    fi
done
echo "Done!"