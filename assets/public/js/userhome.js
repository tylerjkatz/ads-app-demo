//consts
const COOKIE_NAME = "ads_app_demo_auth_token";

//on load
$( document ).ready(function() {
    //read JWT from cookie
    let jwt = getCookieValue(COOKIE_NAME);
    
    //validate cookie
    if(jwt == "") {
        alert("No user login data found.");
        window.location.replace("/unauth.html");
    }

    //get currently logged in user/campaigns and, on success, populate campaigns view
    reqPopUserData(jwt);
});

//fetch current user and populate fields in view
var reqPopUserData = function(token) {
    //AJAX request for current user
    $.ajax({
        url: "/v1/users/me",
        type: "GET",
        dataType: "json",
        headers: {"Authorization": "Bearer " + token}
    }).done(function(user) {
        if(user) {
            //populate remaining user data with AJAX, then present view after promise resolves.
            reqPopUserCampaignData(user.id, token);
        } else {
            //show/log error
            showError(`Could not retrieve currently logged in user. &nbsp;Status ${xhr.status}: ${err}. &nbsp;Click <a href="/">here</a> to return to home page.`);
        }
    }).fail(function() {
        //show/log error
        showError(`Could not retrieve currently logged in user. &nbsp;Status ${xhr.status}: ${err}. &nbsp;Click <a href="/">here</a> to return to home page.`);
    });
}

//request campaigns for user and populate in view
var reqPopUserCampaignData = function(userID, token) {
    //AJAX request for user's campaigns
    $.ajax({
        url: `/v1/users/${userID}/campaigns`,
        type: "GET",
        dataType: "json",
        headers: {"Authorization": "Bearer " + token},
        success: function (campaigns, status, xhr) {
            if(campaigns) {
                if(campaigns.length > 0) {
                    popUserCampaignData(userID, campaigns);
                }
            } else {
                //show/log error
                let errMsg = `Could not retrieve campaigns. &nbsp;Status: ${xhr.status}.`;
                showError(errMsg);
                console.log(errMsg);
            }
        },
        error: function (xhr, status, err) {
            //show/log error
            let errMsg = `Could not retrieve campaigns. &nbsp;Status: ${xhr.status}: ${err}.`;
            showError(errMsg);
            console.log(errMsg);
        }
    }).done(function() {
        presentCampaignsView();
    });
}

//show campaigns ui and hide loading spinner
var presentCampaignsView = function() {
    $("#spinner_container").detach();
    
    //show campaign tabs
    $(".tab").css("visibility", "visible");
}

//populates view with fetched user campaign data
var popUserCampaignData = function(userID, campaigns) {
    if(!campaigns || campaigns.length == 0) {
        console.log("Cannot populate campaigns");
        return;
    }

    //populate view
    for(var campIdx = 0; campIdx < campaigns.length; campIdx++) {
        //add button to campaign_tabs
        let tabBtns = $("#campaign_tabs").html();
        $("#campaign_tabs").html(tabBtns + `
            <button id="c${campIdx}TabBtn" class="ctablinks" onclick="openCampaign(event, '${campIdx}')">${campaigns[campIdx].name}</button>`
        );

        //add content to campaign_container
        let campaignContents = $("#campaign_container").html();
        $("#campaign_container").html(campaignContents + `
            <div id="c${campIdx}_tab_content" class="ctabcontent">
                <p>ID: ${campaigns[campIdx].id}</p>
                <h2>${campaigns[campIdx].name}</h2>
                <p>Goal: ${campaigns[campIdx].goal}</p>
                <p>Total Budget: ${campaigns[campIdx].total_budget}</p>
                <p>Status: ${campaigns[campIdx].status}</p>
                <h3>Platforms:</h3>
                <div id="c${campIdx}_platform_container">
                    <div id="c${campIdx}_platform_tabs" class="tab">
                    </div>
                </div>
            </div>
            `
        );
        
        //populate platform data for this campaign
        popPlatformData(userID, campIdx, campaigns[campIdx].platforms);
    }

    // Click on first campaign as a default
    document.getElementById("c0TabBtn").click();
}

var popPlatformData = function(userID, campIdx, platforms) {
    let platNames = Object.keys(platforms);
    for(var platIdx = 0; platIdx < platNames.length; platIdx++) {
        let platName = platNames[platIdx];
        
        //add button to c${i}_platform_tabs
        let tabBtns = $(`#c${campIdx}_platform_tabs`).html();
        $(`#c${campIdx}_platform_tabs`).html(tabBtns + `
            <button id="c${campIdx}p${platIdx}TabBtn" class="c${campIdx}ptablinks" onclick="openCampaignPlatform(event, '${campIdx}', '${platIdx}')">${platName}</button>`
        );

        //add content to c${i}_platform_container
        let platformContents = $(`#c${campIdx}_platform_container`).html();
        $(`#c${campIdx}_platform_container`).html(platformContents + `
            <div id="c${campIdx}p${platIdx}_tab_content" class="c${campIdx}ptabcontent ptabcontent">
                <h4><u>${platName}</u></h4>
                <p>Status: ${platforms[platName].status}</p>
                <p>Total Budget: ${platforms[platName].total_budget}</p>
                <p>Remaining Budget: ${platforms[platName].remaining_budget}</p>
                <p>Start Date: ${(new Date(Number(platforms[platName].start_date))).toUTCString()}</p>
                <p>End Date: ${(new Date(Number(platforms[platName].end_date))).toUTCString()}</p>
                <h5>Target Audiance:</h5>
                <p id="c${campIdx}p${platIdx}_ta_languages">&emsp; Languages: ${platforms[platName].target_audiance.languages}</p>
                <p id="c${campIdx}p${platIdx}_ta_genders">&emsp; Genders: ${platforms[platName].target_audiance.genders}</p>
                <p id="c${campIdx}p${platIdx}_ta_age_range">&emsp; Age Range: ${platforms[platName].target_audiance.age_range}</p>
                <p id="c${campIdx}p${platIdx}_ta_locations"></p>
                <p id="c${campIdx}p${platIdx}_ta_ints_keys"></p>
                <h5>Creatives:</h5>
                <p id="c${campIdx}p${platIdx}_creatives" style="padding-left:20px"></p>
                <p>&emsp;&emsp; <a href="/v1/users/${userID}/image/${platforms[platName].creatives["image"]}"><img style="max-width:50%;max-height:50%" src="/v1/users/${userID}/image/${platforms[platName].creatives["image"]}" alt="<${platforms[platName].creatives["image"]}>" /></a></p>
                <h5>Insights:</h5>
                <p id="c${campIdx}p${platIdx}_insights"></p>
            </div>
            `
        );

        //populate nested platform objects (TargetAudiance, Creatives, Insights)
        popTargetAudiance(campIdx, platIdx, platforms[platName].target_audiance);
        popCreatives(userID, campIdx, platIdx, platforms[platName].creatives);
        popInsights(campIdx, platIdx, platforms[platName].insights);
    }

    // Click on first campaign as a default
    document.getElementById(`c${campIdx}p0TabBtn`).click();
}

var popTargetAudiance = function(campIdx, platIdx, targetAudiance) {
    if(!targetAudiance) {
        console.log("popTargetAudiance received undefined targetAudiance argument");
    }

    //populate locations
    let locationsContent = "&emsp; Locations: <br/>\n";
    for(idx = 0; idx < targetAudiance.locations.length; idx++) {
        locationsContent += `&emsp;&emsp; ${targetAudiance.locations[idx]} <br/>\n`;
    }
    $(`#c${campIdx}p${platIdx}_ta_locations`).html(locationsContent);

    //populate interests or KeyWords
    if(targetAudiance.interests) {
        //populate interests rather than KeyWords
        let interestsContent = "&emsp; Interests: <br/>\n";
        for(idx = 0; idx < targetAudiance.interests.length; idx++) {
            interestsContent += `&emsp;&emsp; ${targetAudiance.interests[idx]} <br/>\n`;
        }
        $(`#c${campIdx}p${platIdx}_ta_ints_keys`).html(interestsContent);
    } else {
        if(targetAudiance.KeyWords) {
            //populate KeyWords rather than interests
            let keyWordsContent = "&emsp; KeyWords: <br/>\n";
            for(idx = 0; idx < targetAudiance.KeyWords.length; idx++) {
                keyWordsContent += `&emsp;&emsp; ${targetAudiance.KeyWords[idx]} <br/>\n`;
            }
            $(`#c${campIdx}p${platIdx}_ta_ints_keys`).html(keyWordsContent);
        } else {
            //neither was populated
            console.log("popTargetAudiance found no interests OR KeyWords for targetAudiance argument");
        }
    }
}

var popCreatives = function(userID, campIdx, platIdx, creatives) {
    if(!creatives) {
        console.log("popCreatives received undefined creatives argument");
    }

    //populate creatives
    let creativesContent = "\n";

    //declare a map to convert field keys to UI names
    let creativesFieldNameMap = {
        "header": "Header",
        "header_1": "Header 1",
        "header_2": "Header 2",
        "description": "Description",
        "url": "URL",
        "image": "Image",
    }
    
    let creativesKeys = Object.keys(creatives);
    for(idx = 0; idx < creativesKeys.length; idx++) {        
        switch(creativesKeys[idx]) {
            case "url":
                creativesContent += `${creativesFieldNameMap["url"]}: <a href="${creatives["url"]}">${creatives["url"]}</a><br/>\n`;
                break;
            case "image":
                creativesContent += `${creativesFieldNameMap["image"]}: <a href="/v1/users/${userID}/image/${creatives["image"]}">${creatives["image"]}</a><br/>\n`;
                break;
            default:
                creativesContent += `${creativesFieldNameMap[creativesKeys[idx]]}: ${creatives[creativesKeys[idx]]} <br/>\n`;
        }
    }

    //write to html
    $(`#c${campIdx}p${platIdx}_creatives`).html(creativesContent);
}

var popInsights = function(campIdx, platIdx, insights) {
    if(!insights) {
        console.log("popInsights received undefined insights argument");
    }

    //populate insights
    let insightsContent = "\n";

    //declare a map to convert field keys to UI names
    let insightsFieldNameMap = {
        "impressions": "Impressions",
        "clicks": "Clicks",
        "website_visits": "Website Visits",
        "nanos_score": "Nanos Score",
        "cost_per_click": "Cost Per Click",
        "click_through_rate": "Click Through Rate",
        "advanced_kpi_1": "Advanced KPI 1",
        "advanced_kpi_2": "Advanced KPI 2",
    }
    
    let insightsKeys = Object.keys(insights);
    for(idx = 0; idx < insightsKeys.length; idx++) {
        insightsContent += `&emsp; ${insightsFieldNameMap[insightsKeys[idx]]}: ${insights[insightsKeys[idx]]} <br/>\n`;
    }

    //write to html
    $(`#c${campIdx}p${platIdx}_insights`).html(insightsContent);
}

var openCampaign = function(evt, campIdx) {
    // Declare all variables
    var i, ctabcontent, ctablinks;

    // Get all elements with class="ctabcontent" and hide them
    ctabcontent = document.getElementsByClassName("ctabcontent");
    for (i = 0; i < ctabcontent.length; i++) {
        ctabcontent[i].style.display = "none";
    }

    // Get all elements with class="ctablinks" and remove the class "active"
    ctablinks = document.getElementsByClassName("ctablinks");
    for (i = 0; i < ctablinks.length; i++) {
        ctablinks[i].className = ctablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(`c${campIdx}_tab_content`).style.display = "block";
    evt.currentTarget.className += " active";
}

var openCampaignPlatform = function(evt, campIdx, platIdx) {
    // Declare all variables
    var i, ptabcontent, ptablinks;

    // Get all elements with class="ptabcontent" and hide them
    ptabcontent = document.getElementsByClassName(`c${campIdx}ptabcontent`);
    for (i = 0; i < ptabcontent.length; i++) {
        ptabcontent[i].style.display = "none";
    }

    // Get all elements with class="c${campIdx}ptablinks" and remove the class "active"
    ptablinks = document.getElementsByClassName(`c${campIdx}ptablinks`);
    for (i = 0; i < ptablinks.length; i++) {
        ptablinks[i].className = ptablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(`c${campIdx}p${platIdx}_tab_content`).style.display = "block";
    evt.currentTarget.className += " active";
}

var tryLogout = function() {
    //read JWT from cookie
    let jwt = getCookieValue(COOKIE_NAME);
    
    //validate cookie
    if(jwt == "") {
        alert("No user login data found.");
        window.location.replace("/unauth.html");
    }
    
    //AJAX logout request
    $.ajax({
        url: "/v1/users/logout",
        type: "POST",
        dataType: "json",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + jwt
        },
        timeout: 10000,
        complete: function() {
            //redirect to home page
            window.location.replace('/');
        }
    });
}

//read cookie with name 'key' and return value or "" if cookie not set
var getCookieValue = function(key) {
    var name = key + "=";
    var allCookie = decodeURIComponent(document.cookie);
    var cookies = allCookie.split(';');
    for(var i = 0; i < cookies.length; i++) {
        var c = cookies[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

var showError = function(errMsg) {
    //Show errSpan with custom error message
    var errSpan = $("#errSpan");
    errSpan.addClass("err")
    errSpan.html(`ERROR: ${errMsg} <br/>`);
    errSpan.css("visibility", "visible");
}
