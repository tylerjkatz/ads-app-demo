//on ready
$( document ).ready(function() {
    //make err visible
    $("#errSpan").css({
        visibility: "visible"
    })
    
    //sleep for 5 seconds, then redirect home
    setTimeout(function() {
        window.location.replace("/");
    }, 3000);
});
