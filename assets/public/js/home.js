var credCookieName = "ads_app_demo_auth_token"

$( document ).ready(function() {
    var errSpan = $("#errSpan");
    var errMsg = errSpan.html();
    if(errMsg != "") {
        errSpan.html("");
        showError(errMsg);
    }
});

var tryLogin = function() {
    //hide err span if showing
    var errSpan = $("#errSpan");
    errSpan.css({
        visibility: "hidden"
    });

    //hide loading message
    $("#loadingSpan").css({
        visibility: "hidden"
    });
    
    //validate
    var uname = $("#uname").val();
    var pw = $("#pw").val();

    if(uname == "" || pw == "") {
        showError("username and password cannot be blank");
        return;
    }

    //show loading message
    $("#loadingSpan").css({
        visibility: "visible"
    });
    
    //AJAX login request
    $.ajax({
        url: "/login",
        type: "POST",
        dataType: "json",
        headers: {"Content-Type": "application/json"},
        data: JSON.stringify({"username": uname, "password": pw}),
        timeout: 5000,
        success: function (data, status, jqXHR) {
            //hide loading message
            $("#loadingSpan").css({
                visibility: "hidden"
            });
            
            //if token received and matches cookie, login successful
            if(data && data.token) {
                let cookieToken = getCookieValue(credCookieName);
                if(cookieToken == data.token) {
                    //redirect to userhome
                    window.location.replace('/v1/users/home');
                } else {
                    showError(status + ": Login failed.  Inconsistent token between client/server.");
                    console.log("Login failed >> cookie token: " + token + ", response token: " + data.token);
                }
            } else {
                showError("Login request failed with status " + status + ".");
                console.log("Login request failed: " + status + ", " + err);
            }
        },
        error: function (jqXHR, status, err) {
            //hide loading message
            $("#loadingSpan").css({
                visibility: "hidden"
            });

            //normal fail from invalid username/password combo
            showError("Login failed.  Invalid username/password.");
        }
    });
}

//read cookie with name 'cname' and return value or "" if cookie not set
var getCookieValue = function(cname) {
    var name = cname + "=";
    var cookies = decodeURIComponent(document.cookie);
    var ca = cookies.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

var showError = function(errMsg) {
    //Show errSpan with custom error message
    var errSpan = $("#errSpan");
    errSpan.text('ERROR: ' + errMsg);
    errSpan.css({
        visibility: "visible"
    });
}
