<h1> ads-app-demo </h1>

This is a small web app demo of a system that manages ad campaigns for users.  A user can login to see their ad campaign data, provided by an input JSON file.  The data is persisted to a remote PostgreSQL db.   The db tables and backend models are all customized to the input JSON file schema.  So, as long as the schema is maintained, the app will dynamically load any valid set of campiagns from the JSON file.  The backend API is written in Golang using Echo as the web framework.  The UI is composed of HTML, CSS, JavaScript, and jQuery.

<br/>

<h1> Table of Contents </h1>

- [Go Dep and Test](#go-dep-and-test)
- [Startup](#startup)
- [Init Data](#init-data)
- [API](#api)
    - [Source Files](#source-files)
    - [Logging in with Test Admin User](#logging-in-with-test-admin-user)
    - [Logout feature](#logout-feature)
- [Models and DB Schema](#models-and-db-schema)
    - [Client/UI](#clientui)
- [Diff With Starter](#diff-with-starter)
- [Changes for Prod](#changes-for-prod)
- [Acknowledgements](#acknowledgements)

<br/>

# Go Dep and Test
Start off by downloading all outside dependencies and running the included tests.  To do this, simply execute the included shell script:

```
sh test.sh
```

During testing, a mock UserDB service is defined in ```internal/mock/mockdb/user.go``` and utilized to mock database calls.  The actual live UserDB implementation is defined in ```internal/platforms/postgres/user.go```.

<br/>

# Startup
Run the ```main.go``` file in the ```cmd/api/``` directory:

```
go run cmd/api/main.go
```

**NOTE**: The app supports an environment variable called ```ECHO_ENV``` which sets the app execution environment.  However, this can be omitted here since the app defaults to ```dev``` environment, which is what we want for this demo.  Actually, ```ECHO_ENV``` just tells ```main.go``` which config file to use within the ```cmd/api/config/files``` directory.  For example, the default ```dev``` environment would use the file ```config.```**dev**```.yaml```, whereas the environment ```gotime``` would use the file ```config.```**gotime**```.yaml```.

After running the above command, ```main.go``` will run through the app startup sequence which is roughly the following:
1. Gather server/db/middleware info from ```config.dev.yaml```
2. Register domain routing and services
3. Establish a connection to the db
4. Drop all tables from the current db schema
5. Recreate all tables/relations
6. Marshal the provided initial JSON data contained within ```all_init_data/data/data.json``` into Go structs
7. Transform/persist all of that struct data to the db via ```go-pg``` ORM, and finally..
8. Start listening for http requests.

<br/>

# Init Data
As part of the above startup sequence, ```main.go``` creates an admin user that can login and view campaign data.

Nothing is served to the user from ```all_init_data```, which is only used for initialization before the server starts.  Everything is served to the user either from the remote PostgreSQL database (including user images) or the local ```assets``` folder.

The 4 image files located in ```all_init_data/images``` are serialized and persisted to the db as their own ```UserImage``` entities.  The images are stored as byte arrays and served to the user upon request.

Finally, the ```data.json``` file contains a JavaScript array of ad campaign data, which is associated with the created admin user when writing to the db.  The data from ```data.json``` is dynamically read and persisted to the db so campaign info may be added/removed/updated from the JSON prior to startup and the db will reflect this modified data after startup, provided the schema validity is maintained.

Unlike ```data.json```, the admin user and its images are hard-coded and require changes to ```internal/platforms/postgres/pg.go``` to alter that data on startup.  Note that the default config file ```cmd/api/config/files/config.dev.yaml``` indicates all SQL is output via the terminal/console during execution.  This is useful for development.

<br/>

# API
The API docs are provided via SwaggerUI.  With the default ```dev``` environment, SwaggerUI may be viewed by going to ```http://localhost:8080/swaggerui``` in a browser.  The "try it out" functionality does not currently work but it still serves as great visual doc that covers all the important routes for this app.

## Source Files
Some files of high importance to the API are:
```
//server init - static routing, routing groups, db connection config, etc.
cmd/api/server/server.go

//external auth service
cmd/api/service/auth.go

//internal auth service
internal/auth/auth.go

//external user service
cmd/api/service/user.go

//internal user service
internal/user/user.go

//user db logic
internal/platforms/postgres/user.go

//models
internal/*.go
```
The names are pretty straightforward but there are some exceptions.  For example, the ```/logout``` route is supported within the external user service even though ```/login``` is handled in the external auth service.  That decision was made because "logout" is only for use by a verified user, whereas "login" is for general use.

---
## Logging in with Test Admin User
Using Postman or similar tool, send a ```POST``` request with the credentials body below to ```http://localhost:8080/login``` and you will see the response include the JWT as well as a refresh token, which is used to refresh the login session per user (resets expiration to 5 minutes).

Normally, you'd have to include the JWT in an ```Authorization``` header for every authenticated request but since cookie support has been added, all you need to do is send the initial login request and you will be good to go!  The API will get the token from this cookie so you won't need to write it down or anything like that.  However, you will have to note the refresh token if you want to test the ```/v1/users/refresh/:token``` endpoint as a cookie is not currently set for that.

Super Admin Credentials {body}:
```
{
    "username": "admin",
    "password": "nanos"
}
```
You'll be authenticated and able to make any valid API requests for 5 minutes.  You don't have to use the ```Authorization``` header since the JWT middleware has been modified to use a Cookie to track the JWT for the client.  The server first attempts to read the cookie but if that fails, it falls back to the ```Authorization``` header for the token value.  Requests to authenticated endpoints must provide a valid JWT via the cookie or the header to succeed.

---
## Logout feature
For convenience, a logout feature was also added.  This allows for easy user testing.  It works by setting the cookie to be expired immediately and resetting the user's refresh token in the db as well.  There are client-side redirects if the cookie is not present and authenticated views are requested without the cookie.

To logout via the API, send a ```POST``` request to endpoint ```http://localhost:8080/v1/users/logout```

<br/>

# Models and DB Schema
The initial ```all_init_data/data/data.json``` presented some challenges and multiple ways of converting the schema to a relational database such as PostgreSQL.  The challenges were addressed with db relations such as foreign key constraints, compound primary keys, and some entity/struct transformation at the service level when going to or coming from the db.  You can find these entity structs within the ```gitlab.com/tylerjkatz/ads-app-demo/internal/model``` package, which contains at least one model struct per db table.

The following is a summary of each table in the database and how/why it was persisted this way from ```data.json```, using the models within the ```internal``` directory:

<h3><b>Key</b></h3>

<u>table_name:</u>

| col_name_1 | col_name_2 | col_name_3 |
| :---: | :---: | :---: |
| col_1_data_type | col_2_data_type | col_3_data_type |

<br/>

---

<h3><b>Tables</b></h3>

<u>users:</u>

| id(PK) | role_id(FK) | created_at | updated_at | deleted_at | username(unique) | password | last_login | active | token |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| bigint | bigint | timestamp | timestamp | timestamp | text | text | timestamp | boolean | text |

Based on ```user.go```, this table has a one-to-one relation with the ```roles``` table and a one-to-many relationship with the ```campaigns``` table.  The ```username``` column must be unique to not repeat usernames anywhere.  Also, the ```password``` column is stored as a hash of the user's original password.  Finally, the ```token``` column can store the user's refresh auth token if the user is logged in.

<br/>

<u>roles:</u>

| id(PK) | access_level | name |
|:---:|:---:|:---:|
| bigint | smallint | text |

Based on ```role.go```, this table has no other relations besides a one-to-one with ```user```.  The ```smallint``` data type (```int8``` in Golang) was selected to represent ```access_level``` since there should be a small list of possible access levels at any given time.

<br/>

<u>user_images:</u>

| user_id(PK,FK) | name(PK) | created_at | updated_at | deleted_at | jpeg_bytes |
|:---:|:---:|:---:|:---:|:---:|:---:|
| bigint | text | timestamp | timestamp | timestamp | bytea |

Based on ```userImage.go```, this table has a one-to-one relation with ```user```.  Each image essentially belongs to the user rather than the ```platform``` as associating only with each ```platform``` allows endless duplicate assets to be persisted.  Instead, the images are associated with the ```user``` and referenced by ```creatives.image```.  Thus, the primary key for this table is a compound key to prevent more than one occurrence of the combination of ```user_id``` and ```name```.

The ```UserImage``` struct requires transformation going to/from the db.  To persist the images in ```internal/platforms/postgres/pg.go```, the ```UserImage.FileToJpegBytes()``` function is called, passing in the paths/names of the input images and producing the byte array that is persisted.  In the internal user service at ```internal/user/user.go```, the ```UserImage.JpegBytesToImage()``` function is called to create a Go ```JpegImage``` object out of the retrieved byte array.  Finally, in the external user service (```cmd/api/service/user.go```), the ```getUserImage``` handler calls ```UserImage.JpegImageToFile()``` to convert the ```JpegImage``` object to a named file that is stored in a ```tmp``` directory, where it is served to the user.
>A go routine is launched for each user image that is served.  Each go routine waits 10 seconds and then deletes the image from the ```tmp``` dir in order to keep it clean.

<br/>

<u>campaigns:</u>

| id(PK) | user_id(FK) | created_at | updated_at | deleted_at | name | goal | total_budget | status |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| bigint | bigint | timestamp | timestamp | timestamp | text | text | bigint | text |

Based on ```campaign.go```, this table is straightforward with a many-to-one relation with ```users``` and a one-to-many relationship with the ```platforms``` table (see below).

The ```Campaign``` struct requires transformation going to/from the db.  Going to the db from JSON, the campaign's map of ```platforms``` must be converted to a list via the ```Campaign.PlatformMapToList()``` function.  Similarly, going to JSON from the db, the campaign's list of ```platforms``` must be converted back to a map via the ```Campaign.PlatformsToMap()``` function.

<br/>

<u>platforms:</u>

| campaign_id(PK,FK) | name(PK) | target_audiance_id(FK) | creatives_id(FK) | insights_id(FK) | created_at | updated_at | deleted_at | status | total_budget | remaining_budget | start_date | end_date |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| bigint | text | bigint | bigint | bigint | timestamp | timestamp | timestamp | text | bigint | bigint | bigint | bigint |

Based on ```platform.go```, this table has a one-to-one relation with ```target_audiance```, ```creatives```, and ```insights```.  It also has a many-to-one relation with ```campaign```.  Since ```platforms``` is a map within a ```campaign```, there cannot be duplicate combinations of ```campaign_id``` and ```name```.  Thus, the primary key for this table is a compound key composed of ```campaign_id``` and ```name```.

<br/>

<u>target_audiances:</u>

| id(PK) | created_at | updated_at | deleted_at | language_enums | gender_enums | age_range | location_enums | interests | key_words |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| bigint | timestamp | timestamp | timestamp | smallint[] | bigint[] | smallint[] | text[] | text[] |

Based on ```targetAudiance.go```, this table has no other relations besides a one-to-one with ```platform```.  Enumerations were utilized for fields with predefined values, such as ```languages```, ```genders```, and ```locations```.  

These enums are stored in the table as a list of ```smallint``` values, which are mapped back to their predefined ```string``` values before sending to client.  Likewise, the predefined ```string``` values are mapped to their enum values before persisting to db.

<br/>

<u>creatives:</u>

| id(PK) | created_at | updated_at | deleted_at | header | header1 | header2 | description | url | image |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| bigint | timestamp | timestamp | timestamp | text | text | text | text | text | text |

Based on ```creatives.go```, this table has no other relations besides a one-to-one with ```platform```.  It's worth noting that the ```image``` column is essentially an implicit foreign key since its value is used to retrieve the appropriate image from ```user_images```.  However, that constraint was not made explicit to keep the field nullable.

<br/>

<u>insights:</u>

| id(PK) | created_at | updated_at | deleted_at | impressions | clicks | website_visits | nanos_score | cost_per_click | click_through_rate | advanced_kpi1 | advanced_kpi2 |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| bigint | timestamp | timestamp | timestamp | bigint | bigint | bigint | double | double | double | double | double |

Based on ```insights.go```, this table has no other relations besides a one-to-one with ```platform```.  The numeric fields here will never be excluded from unmarshaled JSON output in order to send default (zero) values to client.

<br/>

## Client/UI
There are only 3 screens that the user will see for this demo:
1. Public home (login) page (```assets/public/html/index.html```).
    - Login field validation with visual error messages
    - AJAX login request handling, forwarding to user home on success and showing error(s) on fail.
2. Private user home page which shows the authenticated user's username, date of joining, and ad campaign data (including a preview of their Creatives image in each platform)  (```assets/views/userhome.html```).
    - HTML templating to pass username and date of joining from server to view
    - AJAX to retrieve currently logged in user campaign info, which is shown via dynamically built HTML
        - **The endpoint used here is arguably the most important of the app**  (```/v1/users/:userID/campaigns```) as it joins all appropriate data tables and returns the same array of ad campaigns that was initially provided in ```all_init_data/data/data.json```.
        - Loading animation is presented and kept until final AJAX request for bottom-level entities is complete, providing a smooth UX transition with no downtime.  This can be tested by refreshing the user home page.
        - View is built dynamically and will adjust to the data retrieved
4. Public unauthorized (401) page with a simple red banner which displays the error message and redirects the user to the home page after 5 seconds (```assets/public/html/unauth.html```).

<br/>

# Diff With Starter
A starter project was used as a base for this repo: [gorsk](https://github.com/ribice/gorsk).  All modifications from that starter have been written from scratch with the exception of a few basic HTML/CSS structures borrowed from [W3Schools](https://www.w3schools.com/).

So, to provide complete transparency, branch ```gorsk``` has been provided.  This branch contains the initial version of that project for reference.  More importantly, with both branches, a diff can be done between the branches to discover changes made to the starter quite easily.

To explore all changes made from the starter to the current ```master``` branch, visit the following page on Gitlab: [Compare](https://gitlab.com/tylerjkatz/ads-app-demo/compare/gorsk...downstream%2Fmaster)
>Note: The above [Compare](https://gitlab.com/tylerjkatz/ads-app-demo/compare/gorsk...downstream%2Fmaster) link actually shows the comparison of the ```gorsk``` branch with a branch called ```downstream/master```, which is a clone of the ```master``` branch that was made so that the comparison page would show adds since the starter as green rather than showing them as red subtractions.

<br/>

# Changes for Prod
There are many changes necessary to prepare this app for production use.  For one thing, better authentication with db checking needs to be implemented.  Static content should be served by an outside CMS.  Static scripts should be minified and obfuscated.  More tests need to be added for each model and the new service logic.  A CI/CD pipeline should be implemented that perhaps publishes a Docker image.  There are likely many other changes which can be proposed but these are just a few.

<br/>

# Acknowledgements
Thanks to user ```ribice``` @GitHub for [gorsk](https://github.com/ribice/gorsk) Echo API starter.